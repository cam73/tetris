package main

import (
	tl "github.com/JoelOtter/termloop"
	"math"
)

type CurrentFigure struct {
	collider *Collider
	heap *Heap
	nextFigure *NextFigure
	score *Score
	onGameOverFunc func()

	currentFigure *Figure
	isPaused bool
	isGameOver bool
}

func (cf *CurrentFigure) Draw(s *tl.Screen) {
	if !cf.isPaused {
		for _, coord := range cf.ShadowPoints() {
			s.RenderCell(coord.x, coord.y, &tl.Cell{Ch: '░', Fg: cf.currentFigure.color})
		}
		cf.DrawFigure(cf.currentFigure.location.coords.x, cf.currentFigure.location.coords.y, cf.currentFigure, s)
	}
}

func (cf *CurrentFigure) DrawFigure(x, y int, f *Figure, s *tl.Screen) {
	for _, v := range f.RotatedCoords() {
		s.RenderCell(x + v.x,
			y + v.y,
			&tl.Cell{Ch: '▓', Fg: f.color})
	}
}

func (cf *CurrentFigure) Tick(event tl.Event) {}

func (cf *CurrentFigure) TickFigure() {
	cf.TryMoveDown(true)
}

func (cf *CurrentFigure) Next() {
	cf.collider.DeleteObject(cf.currentFigure)

	cf.currentFigure = cf.nextFigure.figure
	cf.collider.RegistryObject(cf.currentFigure)

	cf.nextFigure.randomNext()
	if cf.collider.IsCollided() {
		cf.isGameOver = true
		if cf.onGameOverFunc != nil {
			cf.onGameOverFunc()
		}
	}
}

func (cf *CurrentFigure) TryMoveRight() {
	cf.currentFigure.MoveRight()
	if cf.collider.IsCollided() {
		cf.currentFigure.setLocationAsPrev()
	}
}

func (cf *CurrentFigure) TryMoveLeft() {
	cf.currentFigure.MoveLeft()
	if cf.collider.IsCollided() {
		cf.currentFigure.setLocationAsPrev()
	}
}

func (cf *CurrentFigure) TryRotateRight() {
	cf.TryRotate(1)
}

func (cf *CurrentFigure) TryRotateLeft() {
	cf.TryRotate(-1)
}

func (cf *CurrentFigure) TryRotate(vector int) {
	cf.currentFigure.Rotate(vector)
	if cf.collider.IsCollided() {
		cf.currentFigure.setLocationAsPrev()
	}
}

func (cf *CurrentFigure) TryDrop() {
	for {
		if !cf.TryMoveDown(true) {
			break
		}
	}
}

func (cf *CurrentFigure) TryMoveDown(merge bool) bool {
	cf.currentFigure.MoveDown()
	if cf.collider.IsCollided() {
		cf.currentFigure.setLocationAsPrev()
		if merge {
			cf.Merge()
			cf.BurnHeap()
			cf.Next()
		}

		return false
	}

	return true
}

func (cf *CurrentFigure) ShadowPoints() []Coord {
	var result []Coord

	distance := cf.heap.coords.y - cf.currentFigure.location.coords.y
	for _, figurePoint := range cf.currentFigure.AbsolutePoints() {
		for _, heapPoint := range cf.heap.AbsolutePoints() {
			if heapPoint.x == figurePoint.x {
				distance = int(math.Min(float64(distance), float64(heapPoint.y - figurePoint.y - 1)))
			}
		}
		distance = int(math.Min(float64(distance), float64(cf.heap.coords.y - figurePoint.y)))
	}

	distance = int(math.Max(float64(distance), 0.0))

	for _, figurePoint := range cf.currentFigure.AbsolutePoints() {
		result = append(result, Coord{figurePoint.x, figurePoint.y + distance})
	}

	return result
}

func (cf *CurrentFigure) Merge() {
	points := cf.currentFigure.AbsolutePoints()
	for _, figurePoint := range points {
		rx := figurePoint.x - cf.heap.coords.x
		ry := cf.heap.coords.y - figurePoint.y
		cf.heap.points = append(cf.heap.points, ColorPoint{point: Coord{rx, ry}, color: cf.currentFigure.color})
	}
	cf.score.ScoreFigure(len(points))
}

func (cf *CurrentFigure) BurnHeap() {
	cf.score.ScoreLines(cf.heap.Burn())
}

func (cf *CurrentFigure) SetPause(isPaused bool) {
	cf.isPaused = isPaused
	cf.heap.SetPause(isPaused)
	cf.nextFigure.SetPause(isPaused)
}

func (cf *CurrentFigure) SetGameOverFunc(gameOverFunc func()) {
	cf.onGameOverFunc = gameOverFunc
}

func NewCurrentFigure(collider *Collider,
	heap *Heap,
	nextFigure *NextFigure,
	score *Score) *CurrentFigure {

	currentFigure := new(CurrentFigure)
	currentFigure.collider = collider
	currentFigure.heap = heap
	currentFigure.nextFigure = nextFigure
	currentFigure.score = score
	currentFigure.currentFigure = currentFigure.nextFigure.figure
	currentFigure.nextFigure.randomNext()
	currentFigure.collider.RegistryObject(currentFigure.currentFigure)
	currentFigure.isPaused = false
	currentFigure.isGameOver = false

	currentFigure.Next()

	return currentFigure
}
