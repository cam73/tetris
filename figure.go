package main

import (
	tl "github.com/JoelOtter/termloop"
)

type Figure struct {
	color tl.Attr
	location *FigureLocation
	prevLocation *FigureLocation
	mask *FigureMask
}

type FigureLocation struct {
	coords *Coord
	rotatePosition int
}

type FigureMask struct {
	points             []Coord
	maxRotatePositions int
}

func (f *Figure) RotatedCoords() []Coord {

	NewPoints := make([]Coord, len(f.mask.points))
	copy(NewPoints, f.mask.points)

	position := f.location.rotatePosition
	if position < 0 {
		position = -1 * (f.mask.maxRotatePositions - position) - 1
	}

	for i := 0; i < position; i++ {
		for k, v := range NewPoints {
			NewPoints[k].x = - v.y
			NewPoints[k].y = v.x
		}
	}

	return NewPoints
}

func (f *Figure) Rotate(vector int) {
	f.setPrevLocationAsCurrent()

	if vector < 0 {
		vector = f.mask.maxRotatePositions + vector % f.mask.maxRotatePositions
	}
	newPosition := f.location.rotatePosition + vector % f.mask.maxRotatePositions
	newPosition = newPosition % f.mask.maxRotatePositions

	f.location.rotatePosition = newPosition
}

func (f *Figure) MoveDown() {
	f.setPrevLocationAsCurrent()
	f.location.coords.y++
}

func (f *Figure) MoveLeft() {
	f.setPrevLocationAsCurrent()
	f.location.coords.x--
}

func (f *Figure) MoveRight() {
	f.setPrevLocationAsCurrent()
	f.location.coords.x++
}

func (f *Figure) setPrevLocationAsCurrent() {
	f.prevLocation.rotatePosition = f.location.rotatePosition
	f.prevLocation.coords.x = f.location.coords.x
	f.prevLocation.coords.y = f.location.coords.y
}

func (f *Figure) setLocationAsPrev() {
	f.location.rotatePosition = f.prevLocation.rotatePosition
	f.location.coords.x = f.prevLocation.coords.x
	f.location.coords.y = f.prevLocation.coords.y
}

func (f *Figure) AbsolutePoints() []Coord {
	var result []Coord

	for _, v := range f.RotatedCoords() {
		result = append(result, Coord{f.location.coords.x + v.x, f.location.coords.y + v.y})
	}

	return result
}

func newFigure(x, y int) *Figure {
	newFigure := new(Figure)
	newFigure.location = new(FigureLocation)
	newFigure.location.coords = new(Coord)
	newFigure.location.coords.x = x
	newFigure.location.coords.y = y
	newFigure.location.rotatePosition = 0
	newFigure.prevLocation = new(FigureLocation)
	newFigure.prevLocation.coords = new(Coord)
	newFigure.mask = new(FigureMask)
	newFigure.color = tl.ColorWhite

	return newFigure
}

func NewFigureJ(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{-1,-1}, Coord{0, -1}, Coord{0, 0}, Coord{0, 1})
	newFigure.mask.maxRotatePositions = 4
	newFigure.color = tl.ColorBlue

	return newFigure
}

func NewFigureL(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{1,-1}, Coord{0, -1}, Coord{0, 0}, Coord{0, 1})
	newFigure.mask.maxRotatePositions = 4
	newFigure.color = tl.RgbTo256Color(240, 160, 0)

	return newFigure
}

func NewFigureI(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{0, -2}, Coord{0, -1}, Coord{0, 0}, Coord{0, 1})
	newFigure.mask.maxRotatePositions = 2
	newFigure.color = tl.ColorWhite

	return newFigure
}

func NewFigureT(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{-1, 0}, Coord{0, 0}, Coord{0, -1}, Coord{1, 0})
	newFigure.mask.maxRotatePositions = 4
	newFigure.color = tl.ColorMagenta

	return newFigure
}

func NewFigureZ(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{-1, -1}, Coord{0, -1}, Coord{0, 0}, Coord{1, 0})
	newFigure.mask.maxRotatePositions = 2
	newFigure.color = tl.ColorRed

	return newFigure
}

func NewFigureS(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{1, -1}, Coord{0, -1}, Coord{0, 0}, Coord{-1, 0})
	newFigure.mask.maxRotatePositions = 2
	newFigure.color = tl.ColorGreen

	return newFigure
}

func NewFigureO(x, y int) *Figure {
	newFigure := newFigure(x, y)
	newFigure.mask.points = append(newFigure.mask.points, Coord{-1, -1}, Coord{0, -1}, Coord{0, 0}, Coord{-1, 0})
	newFigure.mask.maxRotatePositions = 1
	newFigure.color = tl.ColorYellow

	return newFigure
}