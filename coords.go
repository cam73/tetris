package main

type Coord struct {
	x int
	y int
}

func (c Coord) Coords() (int, int) {
	return c.x, c.y
}

type CharPoint struct {
	point Coord
	char  rune
}