package main

import (
	tl "github.com/JoelOtter/termloop"
	"strconv"
)

type Score struct {
	coord Coord

	scores int
	lines int
	level int
}

func (sc *Score) Draw(s *tl.Screen) {
	tl.NewText(sc.coord.x, sc.coord.y,      "╓──────────────────╖", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x, sc.coord.y + 1,  "║ Score:           ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x, sc.coord.y + 2,  "║ Level:           ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x, sc.coord.y + 3,  "║ Lines:           ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x, sc.coord.y + 4,  "║                  ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x, sc.coord.y + 5,  "╙──────────────────╜", tl.ColorWhite, tl.ColorBlack).Draw(s)

	tl.NewText(sc.coord.x + 9, sc.coord.y + 1, strconv.FormatInt(int64(sc.scores), 10), tl.ColorYellow, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x + 9, sc.coord.y + 2, strconv.FormatInt(int64(sc.level), 10), tl.ColorYellow, tl.ColorBlack).Draw(s)
	tl.NewText(sc.coord.x + 9, sc.coord.y + 3, strconv.FormatInt(int64(sc.lines), 10), tl.ColorYellow, tl.ColorBlack).Draw(s)
}

func (sc *Score) Tick(event tl.Event) {}

func (sc *Score) ScoreFigure(countOfPoints int) {
	sc.increaseScores(countOfPoints)
}

func (sc *Score) ScoreLines(countOfLines int) {
	sc.increaseLines(countOfLines)
	sc.increaseScores(countOfLines * 50)
}

func (sc *Score) increaseLines(countOfLines int) {
	sc.lines += countOfLines
}

func (sc *Score) increaseScores(scores int) {
	sc.scores += scores
	sc.applyLevel()
}

func (sc *Score) applyLevel() {
	sc.level = sc.scores / 500 + 1
	if sc.level > 10 {
		sc.level = 10
	}
}

func NewScore(x, y int) *Score {
	newScore := new(Score)
	newScore.coord = Coord{x: x, y: y}
	newScore.scores = 0
	newScore.lines = 0
	newScore.level = 1

	return newScore
}