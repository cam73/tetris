package main

import (
	tl "github.com/JoelOtter/termloop"
)

type Field struct {
	coords *Coord
	width int
	height int
	borders []CharPoint
}

func (f *Field) Draw(s *tl.Screen) {
	for _, v := range f.borders {
		s.RenderCell(f.coords.x + v.point.x, f.coords.y + v.point.y, &tl.Cell{Ch: v.char})
	}
}

func (f *Field) Position() (int, int) {
	return f.coords.x, f.coords.y
}

func (f *Field) Size() (int, int) {
	return f.width, f.height
}

func (f *Field) Tick(event tl.Event) {}

func (f *Field) AbsolutePoints() []Coord {
	var result []Coord

	for _, v := range f.borders {
		result = append(result, Coord{f.coords.x + v.point.x, f.coords.y + v.point.y})
	}

	return result
}

func NewField(x, y, width, height int) *Field {
	newGlass := new(Field)
	newGlass.width = width
	newGlass.height = height
	newGlass.coords = new(Coord)
	newGlass.coords.x = x
	newGlass.coords.y = y
	newGlass.CreateMask()

	return newGlass
}

func (f *Field) CreateMask() {

	for i := 0; i < f.height; i++ {
		f.borders = append(f.borders,
			CharPoint{point: Coord{x: 0, y: i}, char: '│'},
			CharPoint{point: Coord{x: f.width, y: i}, char: '│'})
	}

	for i := 1; i < f.width; i++ {
		f.borders = append(f.borders,
			CharPoint{point: Coord{x: i, y: f.height}, char: '─'})
	}

	f.borders = append(f.borders,
		CharPoint{point: Coord{x: 0, y: f.height}, char: '└'},
		CharPoint{point: Coord{x: f.width, y: f.height}, char: '┘'})
}