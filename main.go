package main

import (
	tl "github.com/JoelOtter/termloop"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	game := tl.NewGame()
	level := tl.NewBaseLevel(tl.Cell{
		Bg: tl.ColorBlack,
		Fg: tl.ColorWhite,
		Ch: ' ',
	})

	collider := NewCollider()
	field := NewField(1, 1, 21, 20)
	heap := NewHeap(2, 20, 20, 20)
	nextFigure := NewNextFigure(25, 5)
	score := NewScore(35, 5)
	currentFigure := NewCurrentFigure(collider, heap, nextFigure, score)
	control := NewControl(25, 1, currentFigure)
	timer := NewTimer(score)

	currentFigure.SetGameOverFunc(func() {
		timer.Stop()
		control.Stop()
		heap.Stop()
	})
	control.RegistryOnPauseFunc(timer.SetPause)
	timer.SetTickFunction(currentFigure.TickFigure)
	collider.RegistryObject(field)
	collider.RegistryObject(heap)

	level.AddEntity(field)
	level.AddEntity(heap)
	level.AddEntity(currentFigure)
	level.AddEntity(nextFigure)
	level.AddEntity(score)
	level.AddEntity(control)

	game.Screen().SetLevel(level)
	timer.Start()
	game.Start()
}

