# Tetris
This is a terminal-based game [Tetris](https://en.wikipedia.org/wiki/Tetris) on [https://github.com/JoelOtter/termloop](Termloop) engine.

![Logo](/docs/project-logo.png)

## Game

![Gameplay](/docs/gameplay.gif)

[Download binary](https://bitbucket.org/cam73/tetris/downloads/)

## Install and run game

### Like package
1. Install [Go](https://golang.org/)
1. Configure [GOPATH](https://github.com/golang/go/wiki/SettingGOPATH)
1. `go get bitbucket.org/cam73/tetris`
1. Enter to `src/bitbucket.org/cam73/tetris` folder
1. Run game `go run .`
1. Build executable binary file `go build .`

### By cloning repository
1. Clone repository
1. Configure [GOPATH](https://github.com/golang/go/wiki/SettingGOPATH)
1. Enter to repository folder
1. Run game `go run .`
1. Build executable binary file `go build .`

### With Docker Compose (Debian GNU/Linux 10 buster)
1. Clone repository
1. Create `docker/.env` based on `docker/.env.dist`
1. Enter to `tetris/docker` folder
1. Compose containers `docker compose up -d`
1. Enter to container `docker-compose exec app bash`
1. Install dependencies `dep ensure`
1. Run game `go run .`
1. Build executable binary file `go build .`

## Contributing
- restarting game
- selecting level
- selecting field size
- auto heap growth
- saving score result
- play original music ([Korobeiniki](https://en.wikipedia.org/wiki/Korobeiniki))

