package main

type Collider struct {
	objects []Object
}

type Object interface {
	AbsolutePoints() []Coord
}

func (f *Collider) RegistryObject(o Object) *Collider {
	f.objects = append(f.objects, o)

	return f
}

func (f *Collider) DeleteObject(o Object) *Collider {
	index := -1

	for i, object := range f.objects {
		if o == object {
			index = i
		}
	}

	if index != -1 {
		f.objects = append(f.objects[:index], f.objects[index+1:]...)
	}

	return f
}

func (f *Collider) IsCollided() bool {
	allPoints := make(map[Coord]bool)

	for _, object := range f.objects {
		for _, coord := range object.AbsolutePoints() {
			if _, ok := allPoints[coord]; ok {
				return true
			}

			allPoints[coord] = true
		}
	}

	return false
}

func NewCollider() *Collider {
	return new(Collider)
}