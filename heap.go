package main

import (
	tl "github.com/JoelOtter/termloop"
	"sort"
	"sync"
)

type Heap struct {
	coords *Coord
	width, height int
	points []ColorPoint

	isPaused bool
	isStopped bool
}

type ColorPoint struct {
	point Coord
	color tl.Attr
}

func (h *Heap) Draw(s *tl.Screen) {
	if h.isPaused {
		str := "PAUSED"
		str2 := "Press any key for resume"
		tl.NewText(h.coords.x + h.width / 2 - len(str) / 2, h.coords.y - h.height / 2 - 2,  str, tl.ColorWhite, tl.ColorBlack).Draw(s)
		tl.NewText(h.coords.x + h.width / 2 - len(str2) / 2, h.coords.y - h.height / 2,  str2, tl.ColorWhite, tl.ColorBlack).Draw(s)
	} else {
		for _, coord := range h.points {
			s.RenderCell(h.coords.x + coord.point.x, h.coords.y - coord.point.y, &tl.Cell{Ch: '█', Fg: coord.color})
		}
		if h.isStopped {
			str := "!!! GAME OVER !!!"
			tl.NewText(h.coords.x + h.width / 2 - len(str) / 2, h.coords.y - h.height / 2 - 2,  str, tl.ColorWhite, tl.ColorBlack).Draw(s)
		}
	}
}

func (h *Heap) AbsolutePoints() []Coord {
	var result []Coord

	for _, coord := range h.points {
		result = append(result, Coord{h.coords.x + coord.point.x, h.coords.y - coord.point.y})
	}

	return result
}

func (h *Heap) Tick(event tl.Event) {}

func (h *Heap) Burn() int {
	var mutex = &sync.Mutex{}
	mutex.Lock()

	burnLineCount := 0

	rows := make(map[int]int)
	for _, coord := range h.points {
		rows[coord.point.y]++
	}

	var removedRows []int
	for row, cnt := range rows {
		if cnt >= h.width {
			burnLineCount++
			h.RemovePointsFromRow(row)
			removedRows = append(removedRows, row)
		}
	}

	sort.Ints(removedRows)

	for k, row := range removedRows {
		for index, coord := range h.points {
			if coord.point.y >= row-k {
				h.points[index].point.y--
			}
		}
	}

	mutex.Unlock()

	return burnLineCount
}

func (h *Heap) RemovePointsFromRow(row int) {
	for index, coord := range h.points {
		if coord.point.y == row {
			h.points = append(h.points[:index], h.points[index+1:]...)
			h.RemovePointsFromRow(row)
			break
		}
	}
}

func (h *Heap) SetPause(isPaused bool) {
	h.isPaused = isPaused
}

func (h *Heap) Stop() {
	h.isStopped = true
}

func NewHeap(x, y, width, height int) *Heap {
	newHeap := new(Heap)
	newHeap.width, newHeap.height = width, height
	newHeap.coords = &Coord{x, y}
	newHeap.isPaused = false
	newHeap.isStopped = false

	return newHeap
}
