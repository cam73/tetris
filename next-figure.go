package main

import (
	tl "github.com/JoelOtter/termloop"
	"math/rand"
)

type NextFigure struct {
	coord Coord
	figure *Figure

	isPaused bool
}

func (nf *NextFigure) Draw(s *tl.Screen) {
	tl.NewText(nf.coord.x, nf.coord.y,      "╓ Next ╖", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(nf.coord.x, nf.coord.y + 1,  "║      ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(nf.coord.x, nf.coord.y + 2,  "║      ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(nf.coord.x, nf.coord.y + 3,  "║      ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(nf.coord.x, nf.coord.y + 4,  "║      ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(nf.coord.x, nf.coord.y + 5,  "╙──────╜", tl.ColorWhite, tl.ColorBlack).Draw(s)

	if nf.isPaused {
		tl.NewText(nf.coord.x, nf.coord.y + 2,  "║PAUSED║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	} else {
		for _, v := range nf.figure.RotatedCoords() {
			s.RenderCell(nf.coord.x+v.x+3,
				nf.coord.y+v.y+3,
				&tl.Cell{Ch: '▓', Fg: nf.figure.color})
		}
	}
}

func (nf *NextFigure) Tick(event tl.Event) {}

func (nf *NextFigure) randomNext() {
	functions := [...]func(x int, y int) *Figure {NewFigureJ,
		NewFigureL,
		NewFigureI,
		NewFigureT,
		NewFigureZ,
		NewFigureS,
		NewFigureO}

	randomIndex := rand.Intn(len(functions))

	newFigure := functions[randomIndex](11, 2)
	if newFigure.mask.maxRotatePositions > 1 {
		rotatePosition := rand.Intn(newFigure.mask.maxRotatePositions)
		newFigure.Rotate(rotatePosition)
	}

	nf.figure = newFigure
}

func (nf *NextFigure) SetPause(isPaused bool) {
	nf.isPaused = isPaused
}

func NewNextFigure(x, y int) *NextFigure {
	nextFigure := new(NextFigure)
	nextFigure.coord = Coord{x: x, y: y}
	nextFigure.randomNext()
	nextFigure.isPaused = false

	return nextFigure
}
