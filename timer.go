package main

import (
	"math"
	"time"
)

type Timer struct {
	tickFunction func()
	score *Score

	durationInMillisecond int
	isPaused              bool
	isStopped             bool
}

func (t *Timer) Start() {
	go t.Tick()
}

func (t *Timer) Tick() {
	for {
		if t.isStopped {
			return
		}
		if !t.isPaused {
			t.tickFunction()
			t.durationInMillisecond = int(math.Max(float64(1500 - (t.score.level - 1) * 200), 50))
			sleepTime := time.Duration(t.durationInMillisecond) * time.Millisecond
			time.Sleep(sleepTime)
		}
	}
}

func (t *Timer) SetTickFunction(tickFunction func()) {
	t.tickFunction = tickFunction
}

func (t *Timer) SetPause(isPaused bool) {
	t.isPaused = isPaused
}

func (t *Timer) Stop() {
	t.isStopped = true
}

func NewTimer(score *Score) *Timer {
	newTimer := new(Timer)
	newTimer.score = score
	newTimer.durationInMillisecond = 1500
	newTimer.isPaused = false
	newTimer.isStopped = false

	return newTimer
}