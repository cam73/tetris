package main

import (
	tl "github.com/JoelOtter/termloop"
	"testing"
)

func TestBurn(t *testing.T) {
	heap := NewHeap(10, 10, 3, 5)
	heap.points = append(heap.points,
		ColorPoint{point: Coord{2, 0}, color: tl.ColorWhite},
		ColorPoint{point: Coord{0, 1}, color: tl.ColorWhite},
		ColorPoint{point: Coord{1, 1}, color: tl.ColorWhite},
		ColorPoint{point: Coord{2, 1}, color: tl.ColorWhite},
		ColorPoint{point: Coord{0, 2}, color: tl.ColorWhite},
		ColorPoint{point: Coord{1, 2}, color: tl.ColorWhite},
		ColorPoint{point: Coord{2, 2}, color: tl.ColorWhite},
		ColorPoint{point: Coord{0, 3}, color: tl.ColorWhite})

	lines := heap.Burn()
	if lines != 2 {
		t.Errorf("Incorrect count of burned lines. Expects: 2, actual: %v", lines)
	}
	if !(len(heap.points) == 2 &&
		heap.points[0].point.x == 2 &&
		heap.points[0].point.y == 0 &&
		heap.points[1].point.x == 0 &&
		heap.points[1].point.y == 1) {
		t.Errorf("Incorrect burn result")
	}
}
