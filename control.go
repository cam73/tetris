package main

import tl "github.com/JoelOtter/termloop"

type Control struct {
	coords Coord
	currentFigure *CurrentFigure

	isStopped   bool
	isPaused    bool
	onPauseFunc func(isPaused bool)
}

func (c *Control) Draw(s *tl.Screen) {
	tl.NewText(c.coords.x, c.coords.y,     "     ═╦═ ╔═ ═╦═ ╔═╗ ╥ ╔═╗  ", tl.ColorYellow, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+1,   "      ║  ╠═  ║  ╠╤╝ ║ ╚═╗  ", tl.ColorYellow, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+2,   "      ╨  ╚═  ╨  ╨└  ╨ ╚═╝  ", tl.ColorYellow, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+11,  "╓───────── Control ──────────╖", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+12,  "║ ← → - move left/right      ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+13,  "║ tab, ↑ - rotate left/right ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+14,  "║ ↓, space - soft/hard drop  ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+15,  "║ enter - music off/on       ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+16,  "║ ^P, ^C - pause/exit        ║", tl.ColorWhite, tl.ColorBlack).Draw(s)
	tl.NewText(c.coords.x, c.coords.y+17,  "╙────────────────────────────╜", tl.ColorWhite, tl.ColorBlack).Draw(s)
}

func (c *Control) Tick(event tl.Event) {
	if c.isStopped {
		return
	}
	if event.Type == tl.EventKey {
		if c.isPaused {
			c.SetPause(false)
		} else {
			switch event.Key {
			case tl.KeyArrowRight:
				c.currentFigure.TryMoveRight()
			case tl.KeyArrowLeft:
				c.currentFigure.TryMoveLeft()
			case tl.KeyArrowUp:
				c.currentFigure.TryRotateRight()
			case tl.KeyTab:
				c.currentFigure.TryRotateLeft()
			case tl.KeyArrowDown:
				c.currentFigure.TryMoveDown(false)
			case tl.KeySpace:
				c.currentFigure.TryDrop()
			// case tl.KeyEnter:
			// music on/off
			case tl.KeyCtrlP:
				c.SetPause(true)
			}
		}
	}
}

func (c *Control) SetPause(isPaused bool) {
	c.isPaused = isPaused
	c.currentFigure.SetPause(isPaused)
	if c.onPauseFunc != nil {
		c.onPauseFunc(isPaused)
	}
}

func (c *Control) Stop() {
	c.isStopped = true
}

func (c *Control) RegistryOnPauseFunc(function func(isPaused bool)) {
	c.onPauseFunc = function
}

func NewControl(x, y int, currentFigure *CurrentFigure) *Control {
	newControl := new(Control)
	newControl.currentFigure = currentFigure
	newControl.coords = Coord{x, y}
	newControl.isPaused = false
	newControl.isStopped = false

	return newControl
}